#!/usr/bin/bash

dbfile=motif_db.csv
path=/home/kevyin/bin/meme/db/motif_databases/


date=`date +%G_%m%d_%H%M%S` 

tmpdb=${date}_db
grep -v "^#" $dbfile | grep "^[a-zA-Z]" > $tmpdb

tmp1=${date}_1
tmp2=${date}_2

cut -f 1 -d',' $tmpdb | sed "s,^,${path},g" | sed "s, , ${path},g" > $tmp1

cut -f 5 -d',' $tmpdb > $tmp2

paste $tmp2 $tmp2 $tmp1 

rm $tmpdb $tmp1 $tmp2
